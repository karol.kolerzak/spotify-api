import spotipy
from spotipy.oauth2 import SpotifyOAuth
from settings import client_id, client_secret, redirect_uri

CLIENT_ID = client_id
CLIENT_SECRET = client_secret
REDIRECT_URI = redirect_uri

scope = "user-modify-playback-state"
sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
                                               redirect_uri=REDIRECT_URI, scope=scope))


def serach_album(query):
    album = sp.search(query, limit=1, type="album")
    album_id = album['albums']['items'][0]['id']
    return album_id


def create_album_uri(album_id):
    return f"spotify:album:" + album_id


def play(context_uri):
    sp.start_playback(context_uri=context_uri)


if __name__ == "__main__":
    while(True):
        album = input("""Enter the album name or write "end" to close the program: """)

        if album == 'end':
            break

        try:
            album_id = serach_album(album)
            album_uri = create_album_uri(album_id)
            play(album_uri)
        except spotipy.SpotifyException:
            print('Active device not found')
        except IndexError:
            print('Album not found')
