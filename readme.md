# Spotify API
The simple app that uses the Spotify API to search for an album and then play it on your device.

## How To Run
Create file settings.py and add

```
CLIENT_ID = "your_client_id"
CLIENT_SECRET = "your_client_secret"
REDIRECT_URI = "your_redirect_uri"
```

## Table of contents
* [Technologies](#technologies)

## Technologies
* Python

